package com.gudy.counter.bean.res;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 0100064695
 * @PROJECT_NAME: counter
 * @DESCRIPTION: 委托成交信息
 * @USER: 涂玄武
 * @DATE: 2020/12/4 16:44
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class TradeInfo {

    /**数据库主键*/
    private int id;

    /**账户ID*/
    private long uid;

    /**股票代码*/
    private int code;

    /**股票名称（关联查询）*/
    private String name;

    /**成交股票流通方向*/
    private int direction;

    /**委托成交股票单股价格*/
    private long price;

    /**委托成交股票数量*/
    private long tcount;

    /**委托订单id*/
    private int oid;

}
