package com.gudy.counter.controller;

import com.gudy.counter.util.DbUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @PROJECT_NAME: counter
 * @DESCRIPTION:
 * @USER: 涂玄武
 * @DATE: 2020/11/20 11:18
 */
@RestController
public class Hello {

    @Autowired
    private StringRedisTemplate template;

    @RequestMapping("/hello")
    public String hello() {
        //return "hello world!";
        return "" + DbUtil.getId();
    }

    @RequestMapping("/hello2")
    public String hello2(){
        template.opsForValue().set("test:Hello","World");
        return template.opsForValue().get("test:Hello");
    }
}
